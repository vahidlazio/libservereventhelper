package com.hungerstation.library.serverevent;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import android.os.Handler;

import com.hungerstation.library.serverevent.interfaces.OnMessageListener;

public class clsServerEventHelper {

	private String Host_Url = "";
	private boolean isOpen = false;
	long mRetry = 3000;
	private String Server_Last_ID_Event = "";
	HashMap<String, OnMessageListener> mMapListeners = new HashMap<>();
	private OnMessageListener mListener;

	public String getLastID() {
		return Server_Last_ID_Event;
	}

	public long getRetry() {
		return mRetry;
	}

	public void setRetry(long _retry) {
		mRetry = _retry;
	}

	public void setOnDataListener(OnMessageListener _Listener) {
		mListener = _Listener;
	}

	public void setServerHost(String _Host) {
		Host_Url = _Host;
	}

	public void parseReceivedMessage(String _Message) {
		try {
			String[] mStrArray = null;
			mStrArray = _Message.split("[\\r\\n]+");
			String mKey = "";
			String mValue = "";
			String mData = "";
			int mDotPosition = -1;
			for (int i = 0; i < mStrArray.length; i++) {
				mDotPosition = mStrArray[i].indexOf(":");
				mKey = mStrArray[i].substring(0, mDotPosition);
				mValue = mStrArray[i].substring(mDotPosition + 1);

				if (mKey != null) {
					if (mKey.length() > 0) {
						if (mKey.equals("retry")) {
							try {
								mRetry = Long.parseLong(mValue);
							} catch (Exception e) {
								// TODO: handle exception
							}
						}
						if (mKey.equals("id")) {
							try {
								Server_Last_ID_Event = mValue;
							} catch (Exception e) {
								// TODO: handle exception
							}
						}
						if (mKey.equals("data")) {
							try {
								mData = mData.concat(mValue);
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
						}
						if (mKey.equals("event")) {
							try {
								if (mMapListeners.containsKey(mValue)) {
									mMapListeners.get(mValue).onMessage(mValue);
								}
							} catch (Exception e) {
								// TODO: handle exception
							}
						}
					} else {
						continue;
					}
				} else {
					continue;
				}
			}
			mListener.onMessage(mData);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	Handler mHandler = new Handler();
	public clsServerEventHelper() {
		// TODO Auto-generated constructor stub
	}

	Runnable mOpenConnectionThread = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			try {
				URL obj = new URL(Host_Url);
				HttpURLConnection con = (HttpURLConnection) obj
						.openConnection();
				con.setInstanceFollowRedirects(false);
				con.setRequestMethod("GET");

				// add request header
				con.setRequestProperty("Accept", "text/event-stream");
				BufferedReader in = new BufferedReader(new InputStreamReader(
						con.getInputStream()));
				
				String mRedirected  = con.getHeaderField("Location");
				if(mRedirected != null)
				{
					isOpen = false;
					in.close();
					Host_Url = mRedirected;
					mHandler.post(mOpenConnectionThread);
					return;
				}
				String response = "";
				int bytesRead = -1;
				char[] buffer = new char[1024];
				while ((bytesRead = in.read(buffer)) >= 0) {
					if (!isOpen) {
						in.close();
						buffer = null;
						return;
					}
					for (int i = 0; i < bytesRead; i++) {
						response = response.concat(Character
								.toString(buffer[i]));
					}
					if (response.contains("\n\n")) {
						parseReceivedMessage(response);
						response = "";
					}
				}
				in.close();
				if (isOpen) {
					Thread.sleep(mRetry);
					(new Thread(mOpenConnectionThread)).start();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				if (isOpen) {
					try {
						Thread.sleep(mRetry);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					(new Thread(mOpenConnectionThread)).start();
				}
			}
		}
	};

	public void addEventListener(String _Key, OnMessageListener _Listener) {
		mMapListeners.put(_Key, _Listener);
	}

	public void openConnection() {
		if(isOpen == false)
		{
		isOpen = true;
		(new Thread(mOpenConnectionThread)).start();
		}	
	}

	public void closeConnection() {
		try {
			isOpen = false;
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public clsServerEventHelper(String _Host_Url) {
		setServerHost(_Host_Url);
	}

}
