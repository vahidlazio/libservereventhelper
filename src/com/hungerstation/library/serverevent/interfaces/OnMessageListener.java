package com.hungerstation.library.serverevent.interfaces;

public interface OnMessageListener {
	public void onMessage(String _Message);
}
